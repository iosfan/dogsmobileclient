//
//  AppCoordinator.swift
//  DogsMobileClient
//
//  Created by paul on 17.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import UIKit

final class AppCoordinator: Coordinator {

    let window: UIWindow
    let tabBarController: UITabBarController
    var childCoordinators = [Coordinator]()
    
    init(window: UIWindow) {
        self.window = window
        self.tabBarController = UITabBarController()
    }
    
    private func configureTabBar() -> [UIViewController] {
        let navConFirstTab = UINavigationController()
        let navConSecondTab = UINavigationController()
        
        let breedsVC = BreedsTableViewController()
        breedsVC.viewModel = BreedsListViewModel()
        breedsVC.delegate = self
        
        let favouritesVC = FavouritesTableViewController()
        favouritesVC.viewModel = FavouritesListViewModel()
        favouritesVC.delegate = self
        
        navConFirstTab.pushViewController(breedsVC, animated: false)
        navConSecondTab.pushViewController(favouritesVC, animated: false)

        return [navConFirstTab, navConSecondTab]
    }
    
    func start() {
        let controllers = configureTabBar()
        tabBarController.viewControllers = controllers

        window.rootViewController = self.tabBarController
        window.makeKeyAndVisible()
    }
}

extension AppCoordinator: BreedsListDelegate {
    func showDetails(for breed: DogBreed) {
        if breed.subBreeds.isEmpty {
            openGallery(for: breed)
            return
        }
        
        let subBreedsVC = SubBreedsTableViewController()
        subBreedsVC.hidesBottomBarWhenPushed = true
        subBreedsVC.viewModel = SubBreedsListViewModel(breed: breed)
        subBreedsVC.delegate = self
        
        (self.tabBarController.selectedViewController as? UINavigationController)?
            .pushViewController(subBreedsVC, animated: true)
    }
    
    func openGallery(for breed: Breed) {
        let imageViewerVC = ImageViewerViewController()
        imageViewerVC.hidesBottomBarWhenPushed = true
        imageViewerVC.viewModel = ImageViewerViewModel(breed: breed)
        imageViewerVC.viewModel.onError = { [weak self] in
            self?.showAlert()
        }

        (self.tabBarController.selectedViewController as? UINavigationController)?
            .pushViewController(imageViewerVC, animated: true)
    }
}

extension AppCoordinator: FavouritesListDelegate {}
extension AppCoordinator: SubBreedsListDelegate {}

extension AppCoordinator {
    func showAlert(title: String = "Some server error", message: String = "Try connect later") {
        let viewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        viewController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.tabBarController.present(viewController, animated: true, completion: nil)
    }
}
