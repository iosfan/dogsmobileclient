//
//  Coordinator.swift
//  DogsMobileClient
//
//  Created by paul on 17.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }

    func start()
}
