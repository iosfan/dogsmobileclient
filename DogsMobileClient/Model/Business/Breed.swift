//
//  Breed.swift
//  DogsMobileClient
//
//  Created by paul on 18.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation

protocol Breed {
    var name: String { get }
    var imagePaths: [String] { get set }
    var description: String { get }
}

struct DogBreed: Breed {
    let name: String
    let subBreeds: [Breed]
    
    var parentBreedName: String? = nil
    var imagePaths: [String]
    
    init(name: String, parentBreedName: String? = nil, subBreeds: [Breed] = [], imagePaths: [String] = []) {
        self.name = name
        self.subBreeds = subBreeds
        self.imagePaths = imagePaths
        self.parentBreedName = parentBreedName
    }
    
    init(response: BreedListReponse.breedDTO) {
        name = response.name
        subBreeds = response.subBreeds.map { DogBreed(name: $0, parentBreedName: response.name) }
        imagePaths = []
    }
}

extension DogBreed {
    var description: String {
        let suffix = subBreeds.count == 1 ? "subbreed" : "subbreeds"
        
        return !subBreeds.isEmpty ? "\(subBreeds.count) \(suffix)" : ""
    }
}
