//
//  FavouriteBreed.swift
//  DogsMobileClient
//
//  Created by paul on 27.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation

struct FavouriteBreed: Breed {
    var name: String
    var imagePaths: [String]
}

extension FavouriteBreed {
    var description: String {
        let suffix = imagePaths.count == 1 ? "photo" : "photos"

        return !imagePaths.isEmpty ? "\(imagePaths.count) \(suffix)" : ""
    }
}
