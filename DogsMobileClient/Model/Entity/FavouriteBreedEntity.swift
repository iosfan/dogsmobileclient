//
//  StoredFavouriteBreed.swift
//  DogsMobileClient
//
//  Created by paul on 28.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation

import Foundation
import RealmSwift

class FavouriteBreedEntity: Object {
    @objc dynamic var name: String = ""
    dynamic var imagePaths = List<String>()
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
    convenience init(favouriteBreed: FavouriteBreed) {
        self.init()

        self.name = favouriteBreed.name
        self.imagePaths.append(objectsIn: favouriteBreed.imagePaths)
    }
    
    func toFavouriteBreed() -> FavouriteBreed {
        return FavouriteBreed(name: self.name, imagePaths: self.imagePaths.toArray())
    }
}
