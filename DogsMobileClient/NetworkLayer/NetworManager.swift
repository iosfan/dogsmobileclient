//
//  NetworManager.swift
//  DogsMobileClient
//
//  Created by paul on 20.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import Moya

enum NetworkError: Error {
    case commonError
}

protocol Network {
    func getBreeds() -> Single<[DogBreed]>
    func getImagePaths(for breed: DogBreed) -> Single<[String]>
}

class NetworkManager: Network {
    private let provider = MoyaProvider<DogsBreedApi>()
    
    func getImagePaths(for breed: DogBreed) -> Single<[String]> {
        let breedName = breed.parentBreedName == nil ? breed.name : breed.parentBreedName! + "/" + breed.name
        
        return provider.rx.request(.breedImages(breed: breedName))
            .filterSuccessfulStatusAndRedirectCodes()
            .map(ImagePathListResponse.self)
            .map { imagePathListResponse in
                return imagePathListResponse.paths.sorted { $0 < $1 }
        }
        .catchError({ error in
            Single<[String]>.error(NetworkError.commonError)
        })
        .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .default))
        .observeOn(MainScheduler.instance)
    }
    
    func getBreeds() -> Single<[DogBreed]> {
        return provider.rx.request(.breeds)
            .filterSuccessfulStatusAndRedirectCodes()
            .map(BreedListReponse.self)
            .map { breedListReponse in
                return breedListReponse.breeds
                    .map { DogBreed(response: $0) }
                    .sorted { $0.name < $1.name }
        }
        .catchError({ error in
            Single<[DogBreed]>.error(NetworkError.commonError)
        })
        .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .default))
        .observeOn(MainScheduler.instance)
    }
}
