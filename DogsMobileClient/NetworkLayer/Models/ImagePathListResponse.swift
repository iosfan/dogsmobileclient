//
//  ImageURLListResponse.swift
//  DogsMobileClient
//
//  Created by paul on 08.09.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation

struct ImagePathListResponse: Decodable {
    let message: [String]
    let status: String
    
    var paths: [String] {
        return message
    }
}
