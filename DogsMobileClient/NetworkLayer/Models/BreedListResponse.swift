//
//  BreedListResponse.swift
//  DogsMobileClient
//
//  Created by paul on 03.09.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation

struct BreedListReponse: Decodable {
    let message: [String : [String]]
    let status: String
    
    struct breedDTO {
        let name: String
        let subBreeds: [String]
    }
    
    var breeds: [breedDTO] {
        return message.map { breedDTO(name: $0.0, subBreeds: $0.1) }
    }
}
