//
//  DogApi.swift
//  DogsMobileClient
//
//  Created by paul on 02.09.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import Moya

public enum DogsBreedApi {
    case breeds
    case breedImages(breed: String)
}

extension DogsBreedApi: TargetType {
    public var baseURL: URL {
        return URL(string: "https://dog.ceo/api")!
    }
    
    public var path: String {
        switch self {
        case .breeds:
            return "/breeds/list/all"
        case .breedImages(let breed):
            return "/breed/\(breed)/images"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .breeds:
            return .get
        case .breedImages(_):
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        return .requestPlain
    }
    
    public var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
}
