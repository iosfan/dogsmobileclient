//
//  StorageManager.swift
//  DogsMobileClient
//
//  Created by paul on 26.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift
import RxRealm

enum StorageType {
    case realm
}

enum DBLayerError: Error {
    case commonError
}

protocol Storage {
    func getFavourites() -> Single<[FavouriteBreed]>
    func addFavourite(breed: FavouriteBreed) -> Completable
    func removeFavourite(breed: FavouriteBreed) -> Completable
    func getFavouriteBreed(by name: String) -> Single<FavouriteBreed?>
}

class StorageManager: Storage {

    var currentStorageType: StorageType = .realm
    
    private var context: StorageContext {
        switch currentStorageType {
        case .realm:
            return RealmStorageContext.shared
        }
    }

    init(storageType: StorageType) {
        self.currentStorageType = storageType
    }
    
    func addFavourite(breed: FavouriteBreed) -> Completable {
        return Completable.create{ [weak self] completable in
            do {
                try self?.context.save(object: FavouriteBreedEntity(favouriteBreed: breed))
                completable(.completed)
            } catch {
                completable(.error(DBLayerError.commonError))
            }
            return Disposables.create()
            
        }
        .catchError { error in
            Completable.error(DBLayerError.commonError)
        }
    }
    
    func removeFavourite(breed: FavouriteBreed) -> Completable {
        return getFavouriteBreedEntity(by: breed.name)
            .do(onSuccess: { [weak self] favouriteBreedEntity in
                guard let favouriteBreedEntity = favouriteBreedEntity else { return }

                try self?.context.delete(object: favouriteBreedEntity)
            }).asCompletable()
            .catchError { error in
                Completable.error(DBLayerError.commonError)
            }
    }
    
    func getFavouriteBreed(by name: String) -> Single<FavouriteBreed?> {
        return getFavouriteBreedEntity(by: name).map { $0?.toFavouriteBreed() }
    }
    
    func getFavouriteBreedEntity(by name: String) -> Single<FavouriteBreedEntity?> {
        return Single<FavouriteBreedEntity?>.create { single in
            let favouriteBreedEntity = self.context
                .fetch(FavouriteBreedEntity.self, predicate: NSPredicate(format: "name == %@", name)).first

            single(.success(favouriteBreedEntity))
            return Disposables.create()
        }
    }
    
    func getFavourites() -> Single<[FavouriteBreed]> {
        return Single<[FavouriteBreed]>.create { [weak self] single in
            let entities = self?.context
                .fetch(FavouriteBreedEntity.self, predicate: nil, sorted: [Sorted(key: "name")]) ?? []
            let favouriteBreeds = entities.map { $0.toFavouriteBreed() }
            
            single(.success(favouriteBreeds))
            return Disposables.create()
        }
    }
}
