//
//  StorageContext.swift
//  DogsMobileClient
//
//  Created by paul on 26.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation

struct Sorted {
    var key: String
    var asc: Bool = true
}

protocol Storable {}

protocol StorageContext {
    func create<T: Storable>(_ model: T.Type, completion: @escaping ((Any) -> Void)) throws

    func save(object: Storable) throws

    func save(object: [Storable]) throws

    func update(block: @escaping () -> ()) throws

    func delete(object: Storable) throws

    func delete(object: [Storable]) throws

    func deleteAll<T: Storable>(_ model: T.Type) throws

    func reset() throws

    func fetch<T: Storable>(_ model: T.Type, predicate: NSPredicate?, sorted: [Sorted]?) -> [T]

    func fetch<T: Storable>(_ model: T.Type, predicate: NSPredicate?, sorted: Sorted?, completion: (([T]) -> ()))
}


extension StorageContext {
    
    func fetch<T: Storable>(_ model: T.Type, predicate: NSPredicate? = nil, sorted: Sorted? = nil, completion: (([T]) -> ()))
    {
        self.fetch(model, predicate: predicate, sorted: sorted, completion: completion)
    }
    
    func fetch<T: Storable>(_ model: T.Type, predicate: NSPredicate? = nil, sorted: [Sorted]? = nil) -> [T]
    {
        return fetch(model, predicate: predicate, sorted: sorted)
    }
    
    func save(object: [Storable]) throws
    {
        try save(object: object)
    }
    
    func save(object: Storable) throws
    {
        try save(object: object)
    }
    
}
