//
//  RealmStorageContext.swift
//  DogsMobileClient
//
//  Created by paul on 26.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RealmSwift

class RealmStorageContext: StorageContext {
    
    var realm: Realm?
    
    static let shared = RealmStorageContext()
    
    private init() {
        realm = try? Realm()
    }
    
    public func safeWrite(_ block: (() throws -> Void)) throws {
        guard let realm = self.realm else {
            throw NSError()
        }
        
        if realm.isInWriteTransaction {
            try block()
        } else {
            try realm.write(block)
        }
        
        realm.refresh()
    }
}


