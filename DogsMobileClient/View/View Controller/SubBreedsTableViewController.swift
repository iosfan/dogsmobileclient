//
//  SubBreedsTableViewController.swift
//  DogsMobileClient
//
//  Created by paul on 21.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol SubBreedsListDelegate: class {
    func openGallery(for breed: Breed)
}

class SubBreedsTableViewController: BaseTableViewController {
    
    private let disposeBag = DisposeBag()
    var viewModel: SubBreedsListViewModel!
    weak var delegate: SubBreedsListDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.title.asDriver()
            .drive(rx.title)
            .disposed(by: disposeBag)
        
        setupUI()
        setupCellTapHandling()
    }
    
    private func setupUI() {
        viewModel.subBreedCells.asDriver().drive(tableView.rx.items) { tableView, index, element in
            let indexPath = IndexPath(item: index, section: 0)
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath) as? CommonTableViewCell else {
                return UITableViewCell()
            }
            cell.viewModel = element
            return cell
        }
        .disposed(by: disposeBag)
    }
    
    private func setupCellTapHandling() {
        tableView
            .rx
            .modelSelected(CommonTableViewCellViewModel.self)
            .subscribe(
                onNext: { [weak self] cellViewModel in
                    let breed = DogBreed(name: cellViewModel.name,
                                         parentBreedName: cellViewModel.parentBreedName,
                                         subBreeds: cellViewModel.subBreeds,
                                         imagePaths: cellViewModel.imagePaths)
                    self?.delegate?.openGallery(for: breed)

                    if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow {
                        self?.tableView?.deselectRow(at: selectedRowIndexPath, animated: true)
                    }
                }
            )
            .disposed(by: disposeBag)
    }
}
