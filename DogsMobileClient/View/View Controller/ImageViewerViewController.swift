//
//  ImageViewerViewController.swift
//  DogsMobileClient
//
//  Created by paul on 23.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ImageViewerViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            let cellNib = UINib(nibName: "ImageViewerCollectionViewCell", bundle: nil)
            collectionView.register(cellNib, forCellWithReuseIdentifier: "ImageViewerCell")
        }
    }
    
    private let disposeBag = DisposeBag()
    var viewModel: ImageViewerViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.title.asDriver()
            .drive(rx.title)
            .disposed(by: disposeBag)
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0

        collectionView.collectionViewLayout = layout
        collectionView.isPagingEnabled = true

        viewModel.prepareDataForCells()
        setupUI()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        let visiblePage = self.collectionView.contentOffset.x / self.collectionView.bounds.size.width
        coordinator.animate(alongsideTransition: { (context) in
            let newOffset = CGPoint(
                x: visiblePage * self.collectionView.bounds.size.width,
                y: self.collectionView.contentOffset.y
            )

            self.collectionView.contentOffset = newOffset
            self.collectionView.collectionViewLayout.invalidateLayout()
            
        })
    }
    
    private func setupUI() {
        collectionView.rx.setDelegate(self).disposed(by: disposeBag)
        
        viewModel.cellViewModels.asDriver().drive(collectionView.rx.items) { collectionView, index, element in
            let indexPath = IndexPath(item: index, section: 0)
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageViewerCell", for: indexPath) as? ImageViewerCollectionViewCell else {
                return UICollectionViewCell()
            }
            cell.viewModel = element
            return cell
        }
        .disposed(by: disposeBag)
    }
}

extension ImageViewerViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

