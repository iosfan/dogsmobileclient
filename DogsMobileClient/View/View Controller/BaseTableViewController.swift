//
//  BaseTableViewController.swift
//  DogsMobileClient
//
//  Created by paul on 21.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import UIKit

class BaseTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let commonCellNib = UINib(nibName: "CommonTableViewCell", bundle: nil)
            tableView.register(commonCellNib, forCellReuseIdentifier: "commonCell")
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    init() {
        super.init(nibName: "BaseTableViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setLoadingIndicator(animating: Bool) {
        animating ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
    }
}
