//
//  BreedsViewController.swift
//  DogsMobileClient
//
//  Created by paul on 18.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol BreedsListDelegate: class {
    func showDetails(for breed: DogBreed)
    func openGallery(for breed: Breed)
}

class BreedsTableViewController: BaseTableViewController {

    private let disposeBag = DisposeBag()
    var viewModel: BreedsListViewModel!
    weak var delegate: BreedsListDelegate?
    
    override init() {
        super.init()
        
        let tabBarItem = UITabBarItem(title: "List", image: UIImage(systemName: "text.justify"), tag: 0)
        self.tabBarItem = tabBarItem
        self.navigationItem.title = "Breeds"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        bindViewModel()
        setupCellTapHandling()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.getBreeds()
    }

    private func bindViewModel() {
        viewModel.breedCells.asDriver().drive(tableView.rx.items) { tableView, index, element in
            let indexPath = IndexPath(item: index, section: 0)
            
            switch element {
            case .normal(let viewModel):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath) as? CommonTableViewCell else {
                    return UITableViewCell()
                }
                cell.viewModel = viewModel
                return cell
            case .error(let message):
                let cell = UITableViewCell()
                cell.isUserInteractionEnabled = false
                cell.textLabel?.text = message
                return cell
            case .empty:
                let cell = UITableViewCell()
                cell.isUserInteractionEnabled = false
                cell.textLabel?.text = "No data available"
                return cell
            }
        }
        .disposed(by: disposeBag)
        
        viewModel
            .loadInProgress
            .asDriver()
            .distinctUntilChanged()
            .drive(onNext: { [weak self] in
                self?.setLoadingIndicator(animating: $0)
            })
            .disposed(by: disposeBag)
    }
    
    private func setupCellTapHandling() {
        tableView
            .rx
            .modelSelected(BreedTableViewCellType.self)
            .subscribe(
                onNext: { [weak self] breedCellType in
                    if case let .normal(viewModel) = breedCellType {
                        let breed = DogBreed(name: viewModel.name,
                                             parentBreedName: viewModel.parentBreedName,
                                             subBreeds: viewModel.subBreeds,
                                             imagePaths: viewModel.imagePaths)
                        self?.delegate?.showDetails(for: breed)
                    }
                    if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow {
                        self?.tableView?.deselectRow(at: selectedRowIndexPath, animated: true)
                    }
                }
            )
            .disposed(by: disposeBag)
    }
}

