//
//  FavouritesViewController.swift
//  DogsMobileClient
//
//  Created by paul on 18.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol FavouritesListDelegate: class {
    func openGallery(for breed: Breed)
}

class FavouritesTableViewController: BaseTableViewController {

    private let disposeBag = DisposeBag()
    var viewModel: FavouritesListViewModel!
    weak var delegate: FavouritesListDelegate?
    
    override init() {
        super.init()
        
        let tabBarItem = UITabBarItem(title: "Favourites", image: UIImage(systemName: "heart"), tag: 1)
        self.tabBarItem = tabBarItem
        self.navigationItem.title = "Favourites"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        bindViewModel()
        setupCellTapHandling()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.title = "Favourites"
        
        viewModel.getFavourites()
    }
    
    private func bindViewModel() {
        viewModel.favouritesCells.asDriver().drive(tableView.rx.items) { tableView, index, element in
            let indexPath = IndexPath(item: index, section: 0)
            
            switch element {
            case .normal(let viewModel):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "commonCell", for: indexPath) as? CommonTableViewCell else {
                    return UITableViewCell()
                }
                cell.viewModel = viewModel
                return cell
            case .error(let message):
                let cell = UITableViewCell()
                cell.isUserInteractionEnabled = false
                cell.textLabel?.text = message
                return cell
            case .empty:
                let cell = UITableViewCell()
                cell.isUserInteractionEnabled = false
                cell.textLabel?.text = "No data available"
                return cell
            }
        }
        .disposed(by: disposeBag)
        
        viewModel
            .loadInProgress
            .asDriver()
            .distinctUntilChanged()
            .drive(onNext: { [weak self] in
                self?.setLoadingIndicator(animating: $0)
            })
            .disposed(by: disposeBag)
    }
    
    private func setupCellTapHandling() {
        tableView
            .rx
            .modelSelected(FavouriteTableViewCellType.self)
            .subscribe(
                onNext: { [weak self] favouriteCellType in
                    if case let .normal(viewModel) = favouriteCellType {
                        let breed: Breed = FavouriteBreed(name: viewModel.name, imagePaths: viewModel.imagePaths)
                        self?.delegate?.openGallery(for: breed)
                    }
                    if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow {
                        self?.tableView?.deselectRow(at: selectedRowIndexPath, animated: true)
                    }
                }
            )
            .disposed(by: disposeBag)
    }
}
