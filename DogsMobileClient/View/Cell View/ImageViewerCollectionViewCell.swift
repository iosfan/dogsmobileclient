//
//  ImageViewerCollectionViewCell.swift
//  DogsMobileClient
//
//  Created by paul on 24.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import UIKit
import Nuke
import RxSwift

class ImageViewerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var buttonAddToFavourites: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let disposeBag = DisposeBag()
    var viewModel: ImageViewerCellViewModel! { didSet { setImage() }}
    
    override func awakeFromNib() {
        super.awakeFromNib()

        buttonAddToFavourites.rx.tap.bind { [weak self] in
            guard let self = self else { return }
            
            self.viewModel.likeOrUnlike()
            if self.viewModel.isFavourite {
                self.animateAndHapticLike()
            }
            self.setLikeButtonStyle()
        }
        .disposed(by: disposeBag)
    }
    
    private func setImage() {
        guard let viewModel = viewModel else { return }
        
        setLikeButtonStyle()
        
        guard let url = URL(string: viewModel.currentImagePath) else {
            imageView.image = UIImage(named: "error")
            viewModel.isImageNotSet = true

            return
        }
        
        activityIndicator.startAnimating()
        Nuke.loadImage(
            with: url,
            options: ImageLoadingOptions(failureImage: UIImage(named: "error")),
            into: imageView,
            completion: { [weak self] result in
                if case .failure(_) = result { viewModel.isImageNotSet = true }
                self?.activityIndicator.stopAnimating()
            }
        )
    }
    
    private func animateAndHapticLike() {
        UIView.animate(
            withDuration: 0.15,
            delay: 0,
            options: [.autoreverse, .allowUserInteraction],
            animations: {
                self.buttonAddToFavourites.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        },
            completion: { (finished) in
                self.buttonAddToFavourites.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
    }
    
    private func setLikeButtonStyle() {
        let imageName = viewModel.isFavourite ? "heart.fill" : "heart"
        let config = UIImage.SymbolConfiguration(pointSize: 40, weight: .regular, scale: .small)
        let symbol = UIImage(systemName: imageName, withConfiguration: config)

        buttonAddToFavourites.setImage(symbol, for: .normal)
        buttonAddToFavourites.backgroundColor = .white
        buttonAddToFavourites.layer.cornerRadius = 25
        buttonAddToFavourites.layer.borderWidth = 2
    }
}


