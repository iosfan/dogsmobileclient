//
//  TableViewCell.swift
//  DogsMobileClient
//
//  Created by paul on 19.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import UIKit

class CommonTableViewCell: UITableViewCell {

    @IBOutlet weak var labelBreedName: UILabel!
    @IBOutlet weak var labelCountInfo: UILabel!
    
    var viewModel: CommonCellViewModel? {
        didSet {
            bindViewModel()
        }
    }
    
    private func bindViewModel() {
        if let viewModel = viewModel {
            labelBreedName?.text = viewModel.name
            labelCountInfo?.text = viewModel.description
        }
    }
}
