//
//  CommonCellViewModel.swift
//  DogsMobileClient
//
//  Created by paul on 19.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation

struct CommonCellViewModel {
    let name: String
    let imageUrls: [String]
    let description: String
    var subBreeds: [Breed] = []
}

extension CommonCellViewModel {
    init(breed: Breed) {
        self.name = breed.name
        self.imageUrls = breed.imageUrls
        self.description = breed.description
        
        if let dogBreed = breed as? DogBreed {
            self.subBreeds = dogBreed.subBreeds
        }
    }
}


