//
//  SubBreedsListViewModel.swift
//  DogsMobileClient
//
//  Created by paul on 21.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SubBreedsListViewModel {
    var subBreedCells: Driver<[CommonCellViewModel]> {
        return cells.asDriver()
    }
    private var cells = BehaviorRelay<[CommonCellViewModel]>(value: [])
    
    init(cellViewModel: CommonCellViewModel) {
        let viewModels = cellViewModel.subBreeds.map { CommonCellViewModel(breed: $0) }
        cells.accept(viewModels)
    }
}
