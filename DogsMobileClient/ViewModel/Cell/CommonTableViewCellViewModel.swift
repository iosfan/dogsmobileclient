//
//  CommonCellViewModel.swift
//  DogsMobileClient
//
//  Created by paul on 19.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation

struct CommonTableViewCellViewModel {
    let name: String
    let imagePaths: [String]
    let description: String
    var parentBreedName: String? = nil
    var subBreeds: [Breed] = []
    var isFavourite: Bool
}

extension CommonTableViewCellViewModel {
    init(breed: Breed) {
        self.name = breed.name
        self.imagePaths = breed.imagePaths
        self.description = breed.description
        
        if let dogBreed = breed as? DogBreed {
            self.parentBreedName = dogBreed.parentBreedName
            self.subBreeds = dogBreed.subBreeds
            self.isFavourite = false
        } else {
            self.isFavourite = true
        }
    }
}


