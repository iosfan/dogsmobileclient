//
//  ImageViewerCellViewModel.swift
//  DogsMobileClient
//
//  Created by paul on 24.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum ImageViewerCellError: Error {
    case common
}

class ImageViewerCellViewModel {
    let currentImagePath: String
    var breed: Breed
    var isFavourite: Bool
    var isImageNotSet: Bool
    var onError: (() -> ())?
    private let storage: Storage
    private let disposeBag = DisposeBag()
    
    init(
        breed: Breed, currentImagePath: String, isFavourite: Bool,
        storage: Storage = StorageManager(storageType: .realm)
    ) {
        self.breed = breed
        self.currentImagePath = currentImagePath
        self.isFavourite = isFavourite
        self.storage = storage
        self.isImageNotSet = false
    }
    
    func likeOrUnlike() {
        guard !isImageNotSet else { return }
        
        if isFavourite {
            removeFavourite()
        } else {
            addFavourite()
        }
    }
    
    private func addFavourite() {
        storage
            .getFavouriteBreed(by: breed.name)
            .flatMapCompletable { [weak self] favouriteBreed in
                guard let self = self else { return Completable.error(ImageViewerCellError.common) }
                
                var favouriteBreed = favouriteBreed
                if favouriteBreed == nil {
                    favouriteBreed = FavouriteBreed(name: self.breed.name, imagePaths: [self.currentImagePath])
                } else {
                    favouriteBreed?.name = self.breed.name
                    favouriteBreed?.imagePaths.append(self.currentImagePath)
                }
                
                return self.storage.addFavourite(breed: favouriteBreed!)
            }
            .subscribe(onCompleted: { [weak self] in
                    self?.isFavourite = true
                }, onError: { [weak self] error in
                    guard !(error is ImageViewerCellError) else { return }

                    self?.onError?()
                }
            )
            .disposed(by: disposeBag)
    }
    
    private func removeFavourite() {
        storage
            .getFavouriteBreed(by: breed.name)
            .flatMapCompletable { [weak self] favouriteBreed in
                guard var favouriteBreed = favouriteBreed, let self = self else { return Completable.error(ImageViewerCellError.common) }
                
                favouriteBreed.imagePaths.removeAll(where: { $0 == self.currentImagePath })
                return favouriteBreed.imagePaths.isEmpty
                    ? self.storage.removeFavourite(breed: favouriteBreed)
                    : self.storage.addFavourite(breed: favouriteBreed)
            }
            .subscribe(onCompleted: { [weak self] in
                    self?.isFavourite = false
                }, onError: { [weak self] error in
                    guard !(error is ImageViewerCellError) else { return }

                    self?.onError?()
                }
            )
            .disposed(by: disposeBag)
    }
}
