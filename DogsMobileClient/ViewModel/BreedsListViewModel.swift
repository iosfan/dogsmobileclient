//
//  BreedsListViewModel.swift
//  DogsMobileClient
//
//  Created by paul on 19.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum BreedTableViewCellType {
    case normal(cellViewModel: CommonCellViewModel)
    case error(message: String)
    case empty
}

class BreedsListViewModel {
    var breedCells: Driver<[BreedTableViewCellType]> {
        return cells.asDriver()
    }
    
    var onShowLoadingProcess: Driver<Bool> {
        return loadInProgress
            .asDriver()
            .distinctUntilChanged()
    }
    
    let networkManager: NetworkManager
    let disposeBag = DisposeBag()
    
    private let loadInProgress = BehaviorRelay(value: false)
    private let cells = BehaviorRelay<[BreedTableViewCellType]>(value: [])
    
    init(networkManager: NetworkManager = NetworkManager()) {
        self.networkManager = networkManager
    }
    
    func getBreeds() {
        loadInProgress.accept(true)
        
        networkManager
            .getBreeds()
            .subscribe { [weak self] event in
                switch event {
                    case .success(let breeds):
                        self?.loadInProgress.accept(false)
                        guard breeds.count > 0 else {
                            self?.cells.accept([.empty])
                            return
                        }
                        
                        self?.cells.accept(breeds.compactMap { .normal(cellViewModel: CommonCellViewModel(breed: $0 )) })
                    case .error(_):
                        self?.loadInProgress.accept(false)
                        self?.cells.accept([
                            .error(
                                message: self?.networkManager.getErrorMessage() ?? "Loading failed, check network connection"
                            )
                        ])
                }
            }
            .disposed(by: disposeBag)
    }
}


