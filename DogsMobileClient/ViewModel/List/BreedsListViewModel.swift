//
//  BreedsListViewModel.swift
//  DogsMobileClient
//
//  Created by paul on 19.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum BreedTableViewCellType {
    case normal(cellViewModel: CommonTableViewCellViewModel)
    case error(message: String)
    case empty
}

class BreedsListViewModel {

    let loadInProgress = BehaviorRelay(value: false)
    let breedCells = BehaviorRelay<[BreedTableViewCellType]>(value: [])
    private let networkManager: NetworkManager
    private let disposeBag = DisposeBag()

    init(networkManager: NetworkManager = NetworkManager()) {
        self.networkManager = networkManager
    }
    
    func getBreeds() {
       loadInProgress.accept(true)
        
       networkManager
            .getBreeds()
            .subscribe(
                onSuccess: { [weak self] breeds in
                    self?.loadInProgress.accept(false)
                    guard breeds.count > 0 else {
                        self?.breedCells.accept([.empty])
                        return
                    }
                    
                    self?.breedCells.accept(breeds.compactMap { .normal(cellViewModel: CommonTableViewCellViewModel(breed: $0 )) })
                },
                onError: { [weak self] _ in
                    self?.loadInProgress.accept(false)
                    self?.breedCells.accept([
                        .error(message: "Loading failed, check network connection")
                    ])
                }
            )
            .disposed(by: disposeBag)
    }
}


