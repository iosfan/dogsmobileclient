//
//  FavouritesListViewModel.swift
//  DogsMobileClient
//
//  Created by paul on 21.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum FavouriteTableViewCellType {
    case normal(cellViewModel: CommonTableViewCellViewModel)
    case error(message: String)
    case empty
}

class FavouritesListViewModel {

    let loadInProgress = BehaviorRelay(value: false)
    let favouritesCells = BehaviorRelay<[FavouriteTableViewCellType]>(value: [])
    private let storage: Storage
    private let disposeBag = DisposeBag()

    init(storage: Storage = StorageManager(storageType: .realm)) {
        self.storage = storage
    }

    func getFavourites() {
        loadInProgress.accept(true)
        
        storage
            .getFavourites()
            .subscribe(onSuccess: { [weak self] favouriteBreeds in
                self?.loadInProgress.accept(false)
                guard favouriteBreeds.count > 0 else {
                    self?.favouritesCells.accept([.empty])
                    return
                }
                
                self?.favouritesCells.accept(favouriteBreeds.compactMap { .normal(cellViewModel: CommonTableViewCellViewModel(breed: $0 )) })
            })
            .disposed(by: disposeBag)
    }
}
