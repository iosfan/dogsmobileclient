//
//  ImageViewerViewModel.swift
//  DogsMobileClient
//
//  Created by paul on 24.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class ImageViewerViewModel {
    var onError: (() -> ())?
    let title = BehaviorRelay<String>(value: "")
    let breed: BehaviorRelay<Breed>
    let isFavourite: Bool
    private let storage: Storage
    private let network: Network
    private let disposeBag = DisposeBag()
    let cellViewModels = BehaviorRelay<[ImageViewerCellViewModel]>(value: [])
    
    func prepareDataForCells() {
        (isFavourite ? Single<[String]>.just([]) : network.getImagePaths(for: breed.value as! DogBreed))
        .asObservable()
        .withLatestFrom(breed) {
            ($0, $1)
        }
        .flatMap { [isFavourite, storage] allImagePaths, breed -> Single<(Breed, FavouriteBreed?)> in
            var breedCopy = breed
            var result = Single<FavouriteBreed?>.just(nil)
            
            if !isFavourite {
                breedCopy.imagePaths = allImagePaths
                result = storage.getFavouriteBreed(by: breed.name)
            }
            
            return result.map { (breedCopy, $0) }
        }
        .map {
            ($0, $1?.imagePaths ?? [])
        }
        .map { [isFavourite, onError] breed, paths  in
            breed.imagePaths.map { path -> ImageViewerCellViewModel in
                let vm = ImageViewerCellViewModel(breed: breed, currentImagePath: path, isFavourite: isFavourite || paths.contains(path))
                vm.onError = onError
                return vm
            }
        }
        .bind(to: cellViewModels)
        .disposed(by: disposeBag)
    }
    
    init(breed: Breed, storage: Storage = StorageManager(storageType: .realm), network: Network = NetworkManager()) {
        self.breed = BehaviorRelay(value: breed)
   
        self.breed
            .map { $0.name }
            .bind(to: self.title)
            .disposed(by: disposeBag)

        self.isFavourite = breed is FavouriteBreed
        self.storage = storage
        self.network = network
    }
}
