//
//  SubBreedsListViewModel.swift
//  DogsMobileClient
//
//  Created by paul on 21.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SubBreedsListViewModel {
    let title = BehaviorRelay<String>(value: "")
    let subBreedCells = BehaviorRelay<[CommonTableViewCellViewModel]>(value: [])
    private let disposeBag = DisposeBag()
    
    init(breed: DogBreed) {
        title.accept(breed.name)
 
        let viewModels = breed.subBreeds.map { CommonTableViewCellViewModel(breed: $0) }
        subBreedCells.accept(viewModels)
    }
}
