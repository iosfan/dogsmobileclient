//
//  FavouritesListViewModel.swift
//  DogsMobileClient
//
//  Created by paul on 21.08.2020.
//  Copyright © 2020 Test Organization. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum FavouriteTableViewCellType {
    case normal(cellViewModel: CommonCellViewModel)
    case error(message: String)
    case empty
}

class FavouritesListViewModel {
    var favouritesCells: Driver<[FavouriteTableViewCellType]> {
        return cells.asDriver()
    }
    
    var onShowLoadingProcess: Driver<Bool> {
        return loadInProgress
            .asDriver()
            .distinctUntilChanged()
    }
    
    let storage: Storage
    let disposeBag = DisposeBag()
    
    private let loadInProgress = BehaviorRelay(value: false)
    private let cells = BehaviorRelay<[FavouriteTableViewCellType]>(value: [])
    
    init(storage: Storage = DBStorage()) {
        self.storage = storage
    }
    
    func getFavourites() {
        loadInProgress.accept(true)
        
        storage
            .getFavourites()
            .subscribe { [weak self] event in
                switch event {
                    case .success(let favouriteBreeds):
                        self?.loadInProgress.accept(false)
                        guard favouriteBreeds.count > 0 else {
                            self?.cells.accept([.empty])
                            return
                        }
                        
                        self?.cells.accept(favouriteBreeds.compactMap { .normal(cellViewModel: CommonCellViewModel(breed: $0 )) })
                    case .error(_):
                        self?.loadInProgress.accept(false)
                        self?.cells.accept([
                            .error(
                                message: self?.storage.getErrorMessage() ?? "Fetching data failed, check db connection"
                            )
                        ])
                }
            }
            .disposed(by: disposeBag)
    }
}
